# TareaUP HorizonteEdificios2d

Implementa un programa que permita ingresar una ciudad conformado por N edificio y muestre como salida gráficamente la línea de horizonte que forma un conjunto de edificios, así como el área y el perímetro de cada uno de ellos.

## Caso de Estudio

Sobre un eje de coordenadas positivo se tienen representados edificios en forma de rectángulos. Cada rectángulo descansa sobre el eje horizonte y se representan por sus abscisas inicial y final y por su altura. La línea de horizonte es el contorno que forman los edificios.

### Entrada
**Una ciudad = {(0; 4; 7); (4; 7; 4); (7; 12; 5); (8; 17; 2); (19; 21; 10)}**

    1. La primera cifra es el eje de abscisas inicial, es decir, donde empieza el rectángulo del edificio.
    2. La segunda cifra es el eje de abscisas final, es decir, donde finaliza el rectángulo del edificio.
    3. La última cifra es la altura del edificio.

### Salida
**Tendremos como resultado una Imagen con el horizonte de los edificios de la ciudad además de los valores del perímetro y el área**

## Proceso de Ejecución
***Ejemplo de entrada:***

`console$ python3 app.py 0,12,10 20,25,25 0,2,14 6,16,12 5,8,15 20,30,11 20,30,11 15,32,8 18,27,17 4,10,5 35,40,20`

*En caso de no ingresar correctamente le dirigirá al menu respectivo de interacción.*

***Ejemplo de salida:***
![datei](salidatarea2UP.png)