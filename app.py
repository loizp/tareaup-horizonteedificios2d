import numpy as np
import cv2

class Diagrama:
    def __init__(self, height = 700, width = 1000):
        # Crea las imagenes en negro
        self.img_hotizonte = np.zeros((height, width, 3), np.uint8)
        self.img_edificios = np.zeros((height, width, 3), np.uint8)
        # declaramos las variables base del tablero
        self.x0, self.x_fin,  = 60, width - 40
        self.y0, self.y_fin = height - 50, 40
        self.eje_color, self.contorno_color = (255, 0, 0), (0, 255, 0)
        self.font_scale = 0.5
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.escala = 20
        self.titulo = "Imagen"

    def add_ejes(self, img):
        # Dibuja una línea horizontal (eje X) verde con un grosor de 1px
        cv2.line(img, (self.x0, self.y0), (self.x_fin, self.y0), self.eje_color, 1)
        # agregamos la punta triangular eje X
        pts = np.array([[self.x_fin, self.y0 - 7], [self.x_fin + 15, self.y0], [self.x_fin, self.y0 + 7]], np.int32)
        cv2.polylines(img, [pts], True, self.eje_color)

        # Dibuja una línea Vertical (eje Y) verde con un grosor de 1px y sus respectivas lineas de métrica
        cv2.line(img, (self.x0, self.y0), (self.x0, self.y_fin), self.eje_color, 1)
        # agregamos la punta triangular eje Y
        pts = np.array([[self.x0 - 7, self.y_fin], [self.x0, self.y_fin - 15], [self.x0 + 7, self.y_fin]], np.int32)
        cv2.polylines(img, [pts], True, self.eje_color)

        self.valores_ejes(img)

    def valores_ejes(self, img):
        # Dibuja las métricas en el eje X
        for i in range(self.x0, self.x_fin, self.escala):
            val = int((i - 60) / self.escala)
            if val % 5 == 0:
                cv2.line(img, (i, self.y0 + 10), (i, self.y0), self.eje_color, 1)
                cv2.putText(img, str(val), (i - 5, 677), self.font, self.font_scale, (255, 255, 255), 1, cv2.LINE_AA)

        # Dibuja las métricas en el eje Y
        for i in range(self.y0, self.y_fin, -self.escala):
            val = int((650 - i) / self.escala)
            if val % 5 == 0:
                cv2.line(img, (self.x0 - 10, i), (self.x0, i), self.eje_color, 1)
                cv2.putText(img, str(val), (15, i + 5), self.font, self.font_scale, (255, 255, 255), 1, cv2.LINE_AA)

    def add_ciudad(self, ciudad):
        for edificio in ciudad.edificios:
            ex0, ex1, ey0, ey1 = edificio.coordenadas()
            # Dibuja los edificios en forma de rectangulo con el color aleatorio de los bordes
            color = (np.random.randint(255), np.random.randint(255), np.random.randint(255))
            cv2.rectangle(self.img_edificios, (ex0, ey0), (ex1, ey1), color, 1)
            print(edificio)

    def mostrar_contorno(self):
        # para detectar los contornos es necesario convertir la imagen en escala de grices
        gray = cv2.cvtColor(self.img_edificios, cv2.COLOR_BGR2GRAY)
        _, th = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
        # extraemos los contornos y sus jerarquias
        contornos, hierarchy = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # procedemos a dibujar los contornos
        for i in range(len(contornos)):
            cv2.drawContours(self.img_edificios, contornos, i, self.contorno_color, 1)
        # agregamos los ejes respectivos y agregamos al panel de muestra
        self.add_ejes(self.img_edificios)
        cv2.imshow(self.titulo, self.img_edificios)

    def mostrar_horizonte(self, ciudad):
        self.add_ciudad(ciudad)
        # conbinamos las imagenes en una sola para no afectar al calculo de contornos
        self.img_hotizonte = cv2.addWeighted(self.img_hotizonte, 1, self.img_edificios, 1, 0)
        # agregamos los ejes respectivos y agregamos al panel de muestra
        self.add_ejes(self.img_hotizonte)
        self.titulo = ciudad.nombre
        cv2.imshow(self.titulo, self.img_hotizonte)

class Ciudad:
    def __init__(self, nombre = "Lima"):
        self.nombre = nombre
        self.edificios = []

    def add_edificios(self):
        # creamos un bucle indefinido para el Menu de registros de edificios
        while True:
            print("\t&&&$$$&&&\tAgregando Edificios\t\t&&&$$$&&&")
            # agregamos una capa de validación de datos de entrada
            try:
                inicio = int(input("Ingrese el inicio: "))
                final = int(input("Ingrese el fin: "))
                if (final <= inicio):
                    print("El valor de finalización debe ser mayor al del inicio")
                else:
                    alto = int(input("Ingrese la altura: "))
                    self.edificios.append(Edificio(inicio, final, alto))
            except:
                print("Solo se permite números enteros positivos")
            print(ciudad)
            print("\n")
            op = input("[s] - Para salir del registro de edificio sino precione otra tecla: ")
            if op in ['s', 'S']:
                break

    def __str__(self):
        entrada = "{"
        for edificio in self.edificios:
            if entrada == "{":
                entrada += "({};{};{})".format(edificio.ini, edificio.fin, edificio.altura)
            else:
                entrada += ";({};{};{})".format(edificio.ini, edificio.fin, edificio.altura)
        entrada += "}"
        return "{} = {}".format(self.nombre, entrada)

class Edificio:
    def __init__(self, ini, fin, altura):
        self.ini = ini
        self.fin = fin
        self.altura = altura

    def coordenadas(self, diagrama = Diagrama()):
        # calculamos las coordenadas en los ejes con los valores base para su ubicación correcta en la imagen
        ex0 = diagrama.x0 + self.ini * diagrama.escala
        ex1 = diagrama.x0 + self.fin * diagrama.escala
        ey0 = diagrama.y0 - self.altura * diagrama.escala
        ey1 = diagrama.y0
        return ex0, ex1, ey0, ey1

    def area_perimetro(self):
        # el perimetro de un rectangulo = 2 * base + 2 * altura
        perimetro = 2 * (self.fin - self.ini) + 2 * (self.altura)
        # el area de un rectangulo = base * altura
        area = (self.fin - self.ini) * (self.altura)
        return perimetro, area

    def __str__(self):
        ex0, ex1, ey0, ey1 = self.coordenadas()
        p, a = self.area_perimetro()
        return "Edificio con las coordenadas ({},{});({},{});({},{});({},{}) - Perímetro = {} y Area = {}".format(
            ex0, ey0, ex0, ey1, ex1, ey0, ex1, ey1, p, a)

def visualizar_imagen(ciudad):
    # visiaualización de la imagen del horizonte
    diagrama = Diagrama()
    diagrama.mostrar_horizonte(ciudad)
    cv2.waitKey(0)
    # visiaualización de la imagen del contorno de los edificios
    diagrama.mostrar_contorno()
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    import sys

    # validamos la ejecución de la aplicación si contiene argumentos
    ciudad = Ciudad()
    if len(sys.argv) > 0:
        for arg in sys.argv:
            if arg != "app.py":
                e = arg.split(',')
                try:
                    if int(e[1]) <= int(e[0]):
                        print("El valor de finalización debe ser mayor al del inicio")
                    else:
                        ciudad.edificios.append(Edificio(int(e[0]), int(e[1]), int(e[2])))
                except:
                    print("Formato de entrada no valida los parametros de entrada son en el siguiente formato y enteros positivos: 1,2,3 3,2,1")
    # si cuenta con los argumentos correctos ejecutamos la visualización de lo contrario muestra un bucle con un menú de interacción
    if len(ciudad.edificios) > 0:
        visualizar_imagen(ciudad)
    else:
        ciudad = None
        while True:
            print("------------------------------  Agregar Edificios ----------------------------------")
            print("\t[a] - Agregar un nuevo edificio en la ciudad")
            print("\t[v] - Ver el horizonte")
            print("\tPresionar cualquier otra tecla para salir del sistema")
            op = input("Ingrese una opción: ")
            if op in ['a', 'A']:
                ciudad = Ciudad(input("Ingrese el nombre de la ciudad: "))
                ciudad.add_edificios()
            elif op in ['v', 'V']:
                if ciudad is None:
                    print("No existe ciudad registrada")
                else:
                    visualizar_imagen(ciudad)
            else:
                break